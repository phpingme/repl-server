Feature: Execution Environment
  In order to be sure that the code that get executed, belongs to me
  I as a trainee
  Need to execute it an isolated environment

Scenario Outline: Execute code in multiple sessions
  Given a code:
  """
  <code>
  """
  And sessions <session_1> <session_2>
  When i execute a given implementation
  Then it occurred in different environment

  Examples:
     | code             | session_1   | session_2 |
     | "foo"<=>"bar"    | 12345       | 54321     |


Scenario Outline: Each session gets it's own home folder and execution file
  Given a shell command:
  """
  <command>
  """
  And session <session>
  When i execute a given command
  Then env has a session specific home folder
  And env has a session specific execution file

  Examples:
     | command           | session    |
     | ls                | 11111      | 
