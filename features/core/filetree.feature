@filetree
Feature: Filetree
 In order to get an overview of project structure
 As a trainee
 I need to get this data


 Scenario: Get Filetree structure
  Given there is a home folder with following files "dir/file2", "dir/afile2", "adir/afile2", "dir/dir2/file3", "Afile1"
  When i call for filetree
  Then i get the given structure


 Scenario: Create a new file
  Given there is a payload to create a new file "newfile"
  When i execute it
  Then there is a file "newfile" in home folder

@readfile
 Scenario: Get file's content
  Given there is a file "file1"
  And it has a content "file1 content"
  And there is a payload to get file "file1" content
  When i execute this payload
  Then i get the file content "file1 content"

@savefile
 Scenario: Save content to file
  Given there is an empty file "file2"
  And there is a payload to save content "<?php $file2" to file "file2"
  When i execute this payload
  Then the file "file2" has content "<?php $file2"

@deletefile
 Scenario: Delete file
    Given there is a file "file3"
    And there is a payload to delete file "file3"
    When i execute this payload
    Then there is no file "file3"

@export
 Scenario: Export filetree
    Given there is a filetree:
    """
    file1
    dir/file2
    """
    And "file1" has content:
    """
    foo
    """
    And "dir/file2" has content:
    """
    bar
    """
    When i execute an export
    Then the export is generated:
"""
[{
  "path":"file1",
  "content":"foo"
},
{
  "path":"dir/file2",
  "content":"bar"
}]
"""
