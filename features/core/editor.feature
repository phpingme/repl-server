Feature: Editor
 In in order to learn an a specific implementation
 As a trainee
 I need to be able to execute it and get a clear response

@onefile
 Scenario: Execute one file logic
   Given there is an implementation:
"""
<?php

declare(strict_types=1);

function product(int $a, int $b){
  return $a*$b;
}

echo product(2, 3);
"""
   When i execute a given implementation
   Then i get the output "6"

@editor
@library
Scenario: Execute one file logic with usage of 3d party library:
  Given there is an implementation:
"""
<?php
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

function stdOutTest(LoggerInterface $logger = null){
  $logger->log(LogLevel::INFO, "test");
}
stdOutTest(new class  implements LoggerInterface {

    use Psr\Log\LoggerTrait;

    public function log($level, $message, array $context = array())
    {
        echo $message;
    }
});
"""
  When i execute a given implementation
  Then i get the output "test"
