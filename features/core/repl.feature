Feature: REPL
 In in order to learn a command
 As a trainee
 I need to be able to see output of this command

@repl
Scenario Outline: Execute simple expression
  Given there is a command:
  """
   <expression>
  """
  When i execute a given command
  Then i get the output <output>

  Examples:
     | expression       | output   |
     | "foo"<=>"bar"    | 'int(1)' |
     | 5.6 <=> 7;       | 'int(-1)' |
