Feature: REPL Error
 In in order to get feedback about wrong implementations
 As a trainee
 I need to see error output in a repl

 Scenario: execute incorrect code from editor
   Given there is an implementation:
"""
<?php

interface A
 {
   public function createStack() : SplStack
   {
      return new SplStack();
   }
 }
"""
   When i execute a given implementation
   Then i get the error message


 Scenario: execute non parsable code from editor
   Given there is an implementation:
"""
<?php

interface A
 {
   public function createStack() : SplStack
 }
"""
   When i execute a given implementation
   Then i get the error message
