Feature: Terminal
 In in order to execute a shell command
 As a trainee
 I need to be able to type it and get a relevant output for it

@terminal @shell
Scenario Outline: Execute a shell command
  Given there is a shell command:
  """
   <command>
  """
  When i execute a given command
  Then i get the output <output>

  Examples:
     | command              | output   |
     | touch test && ls     | test     |

@terminal @commands
Scenario Outline: Shell Command have to be predefined
   Given there is a shell command:
   """
   <command>
   """
   And predefined meta:
   """
   <meta>
   """
   When i execute a given command
   Then i get the multiline output with <output>

   Examples:
    | command               | meta                               | output   |
    | vendor/bin/behat      | {"commands":["vendor/bin/behat"]}  | '[Behat\Behat\Context\Exception\ContextNotFoundException]'|
    | vendor/bin/php-parse  | {"commands":["vendor/bin/behat"]}  | 'sh: 1: vendor/bin/php-parse: not found'  |
