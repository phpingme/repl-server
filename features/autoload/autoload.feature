Feature:
  In order to be able to autoload class from another file
  As a trainee
  I need a session based autoloading

Scenario Outline: Editor mode
  Given i have a session <session>
  Given file "Foo.php" with content:
"""
<?php
class Foo {
  public function __construct(){
    echo __CLASS__;
  }
}
"""
  And file "Bar.php" with content:
"""
<?php
  class Bar extends Foo { }
"""
  When i execute in <mode> mode with code:
"""
<code>
"""
  Then i get "Foo" back

  Examples:
   | mode          | code                                                    | session |
   | 'editor'      | <?php return new Bar();                                 | editor_session |
   | 'terminal'    | php -r \"require('./vendor/autoload.php'); new Bar();\" | terminal_session |
