<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FiletreeContext implements Context, SnippetAcceptingContext
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $this->executor = new \Phpingme\ReplServer\EvaluationExecutor();
    }

    /**
     * @Given there is a home folder with following files :arg1, :arg2, :arg3, :arg4, :arg5
     */
    public function thereIsAHomeFolderWithFollowingFiles($arg1, $arg2, $arg3, $arg4, $arg5)
    {
        $sampleData = ['session'=>'filetree'];
        $env = new \Phpingme\ReplServer\SandboxEnv($sampleData['session']);
        $env->createFile($arg1);
        $env->createFile($arg2);
        $env->createFile($arg3);
        $env->createFile($arg4);
        $env->createFile($arg5);
    }

    /**
     * @When i call for filetree
     */
    public function iCallForFiletree()
    {
        $sampleData = ['session'=>'filetree', 'mode'=>'filetree', 'code'=>['action'=>'filetree']];
        $this->result = call_user_func($this->executor, $sampleData);
    }

    /**
     * @Then i get the given structure
     */
    public function iGetTheGivenStructure()
    {
        $filetree = json_decode($this->result[0], true);
        PHPUnit_Framework_Assert::assertEquals(array (
        0 =>
        array (
          'name' => 'adir',
          'type' => 'dir',
          'children' =>
          array (
            0 =>
            array (
              'name' => 'afile2',
              'type' => 'file',
            ),
          ),
        ),
        1 =>
        array (
          'name' => 'dir',
          'type' => 'dir',
          'children' =>
          array (
            0 =>
            array (
              'name' => 'dir2',
              'type' => 'dir',
              'children' =>
              array (
                0 =>
                array (
                  'name' => 'file3',
                  'type' => 'file',
                ),
              ),
            ),
            1 =>
            array (
              'name' => 'afile2',
              'type' => 'file',
            ),
            2 =>
            array (
              'name' => 'file2',
              'type' => 'file',
            ),
          ),
        ),
        2 =>
        array (
          'name' => 'Afile1',
          'type' => 'file',
        ),
      ),  $filetree);
    }

    /**
     * @Given there is a payload to create a new file :arg1
     */
    public function thereIsAPayloadToCreateANewFile($arg1)
    {
      $payload = ['code'=> ['path'=>$arg1, 'action'=>'CREATE_FILE'],'session'=>'create_file', 'mode'=>'filetree'];
      $this->payload = $payload;
    }

    /**
     * @When i execute it
     */
    public function iExecuteIt()
    {
      $this->result = call_user_func($this->executor, $this->payload);
    }

    /**
     * @Then there is a file :arg1 in home folder
     */
    public function thereIsAFileInHomeFolder($arg1)
    {
        $env = new \Phpingme\ReplServer\SandboxEnv($this->payload['session']);
        PHPUnit_Framework_Assert::assertTrue($env->fileExists($arg1));
    }

    /**
     * @Given there is a file :arg1
     */
    public function thereIsAFile($arg1)
    {
       $this->payload = ['session'=>'read_file'];
       $this->file = $arg1;
       $this->env = new \Phpingme\ReplServer\SandboxEnv($this->payload['session']);
       $this->env->createFile($arg1);
    }

    /**
     * @Given it has a content :arg1
     */
    public function itHasAContent($arg1)
    {
      $this->env->saveFileContent($this->file, $arg1);
    }

    /**
     * @Given there is a payload to get file :arg1 content
     */
    public function thereIsAPayloadToGetFileContent($arg1)
    {
        $this->payload['code'] = ['action'=>'read_file', 'path'=>$arg1];
        $this->payload['mode'] = 'filetree';

    }

    /**
     * @When i execute this payload
     */
    public function iExecuteThisPayload()
    {
        $this->result = call_user_func($this->executor, $this->payload);
    }

    /**
     * @Then i get the file content :arg1
     */
    public function iGetTheFileContent($arg1)
    {
      $content = json_decode($this->result[0], true);
      PHPUnit_Framework_Assert::assertEquals($arg1, $content);
    }

    /**
     * @Given there is an empty file :arg1
     */
    public function thereIsAnEmptyFile($arg1)
    {
       $this->payload = ['session'=>'save_file'];
       $this->file = $arg1;
       $this->env = new \Phpingme\ReplServer\SandboxEnv($this->payload['session']);
    }


    /**
     * @Given there is a payload to save content :arg1 to file :arg2
     */
    public function thereIsAPayloadToSaveContentToFile($arg1, $arg2)
    {
      $this->payload['code'] = ['action'=>'save_file', 'path'=>$arg2, 'content'=>$arg1];
      $this->payload['mode'] = 'filetree';

    }

    /**
     * @Then the file :arg1 has content :arg2
     */
    public function theFileHasContent($arg1, $arg2)
    {
      $content = $this->env->readFile($arg1);
      PHPUnit_Framework_Assert::assertEquals($arg2, $content);
    }


    /**
     * @Given there is a payload to delete file :arg1
     */
    public function thereIsAPayloadToDeleteFile($arg1)
    {
      $this->payload['code'] = ['action'=>'delete_file', 'path'=>$arg1];
      $this->payload['mode'] = 'filetree';
    }

    /**
     * @Then there is no file :arg1
     */
    public function thereIsNoFile($arg1)
    {
        PHPUnit_Framework_Assert::assertFalse($this->env->fileExists($arg1));
    }

    /**
     * @Given there is a filetree:
     */
    public function thereIsAFiletree(PyStringNode $string)
    {
        $this->env = new \Phpingme\ReplServer\SandboxEnv('export_filetree');
        array_walk($string->getStrings(), function($path) use ($env){
          $this->env->createFile($path);
        });
    }

    /**
     * @Given :arg1 has content:
     */
    public function hasContent($arg1, PyStringNode $string)
    {
        $this->env->saveFileContent($arg1, $string->__toString());
    }

    /**
     * @When i execute an export
     */
    public function iExecuteAnExport()
    {
        $payload = ['code'=> ['action'=>'EXPORT_FILETREE'],'session'=>'export_filetree', 'mode'=>'filetree'];
        $this->result = (new \Phpingme\ReplServer\EvaluationExecutor())($payload);
    }

    /**
     * @Then the export is generated:
     */
    public function theExportIsGenerated(PyStringNode $string)
    {
      $expected = array (
         1 =>
         array (
           'path' => '/file1',
           'content' => 'foo',
         ),
         0 =>
         array (
           'path' => '/dir/file2',
           'content' => 'bar',
         ),
     );
     PHPUnit_Framework_Assert::assertEquals($expected, json_decode($this->result[0], true));
   }
}
