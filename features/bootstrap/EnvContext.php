<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use \Phpingme\ReplServer\SandboxEnv;

/**
 * Defines application features from the specific context.
 */
class EnvContext implements Context, SnippetAcceptingContext
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $this->msgInterp = new \Phpingme\ReplServer\MessageInterpreter();
    }




    /**
     * @Given a code:
     */
    public function aCode(PyStringNode $string)
    {
      $json = '{"op":"eval","session":null}';

      $data = json_decode($json, true);
      $data['code'] = (string)$string;

      $this->payloads[0] = $data;
      $this->payloads[1] = $data;
    }

    /**
     * @Given sessions :arg1 :arg2
     */
    public function sessions($arg1, $arg2)
    {
        $this->payloads[0]['session'] = $arg1;
        $this->payloads[1]['session'] = $arg2;
    }




    /**
     * @When i execute a given implementation
     */
    public function iExecuteAGivenImplementation()
    {
        call_user_func($this->msgInterp, json_encode($this->payloads[0]));
        call_user_func($this->msgInterp, json_encode($this->payloads[1]));
    }

    /**
     * @Then it occurred in different environment
     */
    public function itOccurredInDifferentEnvironment()
    {
        $pathMask = SandboxEnv::FILE_PATH_MASK;
        $execFilePath_1 = sprintf($pathMask, $this->payloads[0]['session']);
        $execFilePath_2 = sprintf($pathMask, $this->payloads[1]['session']);
        PHPUnit_Framework_Assert::assertNotEquals($execFilePath_2, $execFilePath_1);
    }


    /**
     * @Given a shell command:
     */
    public function aShellCommand(PyStringNode $string)
    {
      $json = '{"op":"eval","mode":"terminal", "session":null}';

      $data = json_decode($json, true);
      $data['code'] = (string)$string;

      $this->payload = $data;
    }

    /**
     * @Given session :arg1
     */
    public function session($arg1)
    {
        $this->payload['session'] = $arg1;
    }

    /**
     * @When i execute a given command
     */
    public function iExecuteAGivenCommand()
    {
        $this->output = call_user_func($this->msgInterp, json_encode($this->payload));
    }


    /**
     * @Then env has a session specific home folder
     */
    public function envHasASessionSpecificHomeFolder()
    {
      $pathFolderMask = SandboxEnv::FOLDER_PATH_MASK;
      $homeFolderPath = sprintf($pathFolderMask, $this->payload['session']);

      PHPUnit_Framework_Assert::assertTrue(is_dir($homeFolderPath), 'env has to have a session specific home folder');
    }

    /**
     * @Then env has a session specific execution file
     */
    public function envHasASessionSpecificExecutionFile()
    {
      $pathFileMask = SandboxEnv::FILE_PATH_MASK;
      $execFilePath = sprintf($pathFileMask, $this->payload['session']);

      PHPUnit_Framework_Assert::assertTrue(file_exists($execFilePath));
    }

}
