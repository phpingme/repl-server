<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use \Phpingme\ReplServer\SandboxEnv;

/**
 * Defines application features from the specific context.
 */
class AutoloadContext implements Context, SnippetAcceptingContext
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $this->executor = new \Phpingme\ReplServer\EvaluationExecutor();
    }


    /**
     * @Given i have a session :arg1
     */
    public function iHaveASession($arg1)
    {
        $this->session = $arg1;
    }

    /**
     * @Given file :arg1 with content:
     */
    public function fileWithContent($arg1, PyStringNode $string)
    {
      $env = (new \Phpingme\ReplServer\SandboxEnv($this->session));
      $env->createFile($arg1);
      $env->saveFileContent($arg1, $string->__toString());
    }

    /**
     * @When i execute in :arg1 mode with code:
     */
    public function iExecuteInModeWithCode($arg1, PyStringNode $string)
    {
      $this->result = call_user_func($this->executor, [
        'session' => $this->session,
        'mode' => $arg1,
        'code' => $string->__toString(),
        ]);
    }

    /**
     * @Then i get :arg1 back
     */
    public function iGetBack($arg1)
    {
        PHPUnit_Framework_Assert::assertEquals($this->result[0], $arg1);
    }
}
