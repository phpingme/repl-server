<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context, SnippetAcceptingContext
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $this->msgInterp = new \Phpingme\ReplServer\MessageInterpreter();
    }

    /**
     * @Given there is an implementation:
     */
    public function thereIsAnImplementation(PyStringNode $string)
    {
        $json = '{"op":"eval","mode":"editor","session":null}';
        $data = json_decode($json, true);
        $data['code'] = (string)$string;
        $this->payload = json_encode($data);
    }

    /**
     * @Given there is a shell command:
     */
    public function thereIsAShellCommand(PyStringNode $string)
    {
        $json = '{"op":"eval","mode":"terminal","session":"terminal_mode"}';
        $data = json_decode($json, true);
        $data['code'] = (string)$string;
        $this->payload = json_encode($data);
    }


     /**
      * @Given there is a command:
      */
    public function thereIsACommand(PyStringNode $string)
    {
          $json = '{"op":"eval","mode":"repl","session":null}';

          $data = json_decode($json, true);
          $data['code'] = (string)$string;
          $this->payload = json_encode($data);
    }

     /**
      * @Given predefined meta:
      */
    public function predefinedMeta(PyStringNode $string)
    {
      $data = json_decode($this->payload, true);
      $data['meta'] = json_decode((string)$string, true);
      $this->payload = json_encode($data);
    }

    /**
     * @When i execute a given implementation
     */
    public function iExecuteAGivenImplementation()
    {
      $mi = $this->msgInterp;
      $this->output = $mi( $this->payload);
    }

    /**
     * @Then i get the output :arg1
     */
    public function iGetTheOutput($arg1)
    {
      $output = json_decode($this->output, true);
      PHPUnit_Framework_Assert::assertEquals($arg1, $output[0]["value"][0]);
    }


    /**
     * @When i execute a given command
     */
    public function iExecuteAGivenCommand()
    {
      $mi = $this->msgInterp;
      $this->output = $mi( $this->payload);
    }

    /**
     * @Then i get the output `int(:arg1)`
     */
    public function iGetTheOutputInt($arg1)
    {
        $output = json_decode($this->output, true);
        PHPUnit_Framework_Assert::assertEquals($arg1, $output[0]["value"][0]);
    }


    /**
     * @Then i get the error message
     */
    public function iGetTheErrorMessage()
    {
        $output = json_decode($this->output, true);
        PHPUnit_Framework_Assert::assertEmpty($output[0]["value"][0]);
        PHPUnit_Framework_Assert::assertNotEmpty($output[0]["value"][1]);
    }



    /**
     * @Then i get the multiline output with :arg1
     */
    public function iGetTheMultilineOutputWith($arg1)
    {
      $output = json_decode($this->output, true);
      $multiLineOuput = implode( $output[0]["value"]);

      PHPUnit_Framework_Assert::assertNotEmpty(strstr($multiLineOuput, $arg1));
    }

}
