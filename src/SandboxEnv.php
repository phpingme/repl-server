<?php

namespace Phpingme\ReplServer;

class SandboxEnv extends \ArrayObject
{
  const FILE_PATH_MASK = "/tmp/execute_%s.php";
  const AUTOLOAD_FILE_PATH_MASK = "/tmp/home_%s/vendor/autoload.php";
  const FOLDER_PATH_MASK = "/tmp/home_%s";

  public function __construct($session){
      $env = $this->touchExectionEnv($session);
      parent::__construct($env, \ArrayObject::ARRAY_AS_PROPS);
  }


  private function touchExectionEnv($session){
    $envFolder = sprintf(self::FOLDER_PATH_MASK, $session);
    if(!is_dir($envFolder) && !mkdir($envFolder)){
        throw new \Error("Huston has a problem");
    }

    $envFile = sprintf( self::FILE_PATH_MASK, $session);
    if(!file_exists($envFile) && !touch($envFile)){
        throw new \Error("Huston has a problem");
    }

    $envAutoloadFile = sprintf( self::AUTOLOAD_FILE_PATH_MASK, $session);
    $this->ensureDirExists($envAutoloadFile);
    if(!file_exists($envAutoloadFile) && !touch($envAutoloadFile)){
        throw new \Error("Huston has a problem");
    }

    // ensure file is clean
    file_put_contents($envFile, "<?php ");

    $this->createAutoloadingFile($envAutoloadFile, $envFolder);

    return ['execution_file'=>$envFile, 'home_folder'=>$envFolder, 'autoloading_file'=>$envAutoloadFile];
  }

  private function createAutoloadingFile($autoloading_file, $home_folder){
    file_put_contents(
      $autoloading_file,
      '<?php $loader=require("'.getcwd().'/vendor/autoload.php");$loader->setPsr4("", "'.$home_folder.'");'
    );
  }

  public function initExecFile($content=""){
    file_put_contents($this->execution_file, "<?php ".$content);
  }


  public function symlinkCommands($commands){

    $dir = $this->home_folder;
    array_map(function($command) use ($dir){


      $from = getcwd() . "/$command";
      $to = $dir . "/" . $command;
      if(file_exists($to)){
        return;
      }
      $this->ensureDirExists($to);
      exec('ln -s '.$from.' '.$to);
    }, $commands);

  }

  protected function ensureDirExists($to){

    $commandDir = substr($to, 0, strrpos($to, "/"));

    if(!is_dir($commandDir)){
      $folderStack = explode("/", $commandDir);
      $path = "";
      $checkedFolderStack = [];
      while(null !== $folder = array_shift($folderStack)){
        $path = $path . "/" . $folder;
        if(!is_dir($path) && !mkdir($path, 0755, true)){
          throw new \Error("Fuston has a problem");
        }
      }
    }
  }

  public function fileExists($path){
    return file_exists($this->home_folder . '/'.$path);
  }

  public function createFile($path)
  {
     $fullPath = $this->home_folder . '/'.$path;
     $this->ensureDirExists($fullPath);
     return touch($fullPath);
  }

  public function deleteFile($path)
  {
     $fullPath = $this->home_folder . '/'.$path;
     return unlink($fullPath);
  }


  public function readFile($path)
  {
    $fullPath = $this->home_folder . '/'.$path;
    return file_get_contents($fullPath);
  }

  public function saveFileContent($path, $content)
  {
    $fullPath = $this->home_folder . '/'.$path;
    $this->ensureDirExists($fullPath);
    return file_put_contents($fullPath, $content);
  }

  public function appendToExecutionFile($content)
  {
    file_put_contents($this->execution_file, $content, FILE_APPEND);
  }

  public function serializeFileTree($allowVendor = false)
  {
    $serializedFileTree = [];
    $this->buildFileTree(null, function($tuple, $tuplePath) use (&$serializedFileTree){
      if(!$allowVendor && strpos($tuplePath, '/vendor/')>0){
        return;
      }
      if(is_file($tuplePath)){
        $serializedFileTree[] = [
          'path' => substr($tuplePath, strlen($this->home_folder)),
          'content' => file_get_contents($tuplePath),
        ];
      }
    });
    return $serializedFileTree;
  }

  /**
   * function puted here just becouse of autoloading, have to be completely context independent
   */
  public  function buildFileTree($dir=null, callable $cb = null) {
    if(null === $dir){
      $dir = $this->home_folder;
    }

    $tree = $this->buildTree($dir, $cb);
    $cleanTree = $this->cleanUpEmptyFolders($tree);
    $foldersFirstTree = $this->sortFoldersFirst($cleanTree);

    return $foldersFirstTree;
  }

  private function buildTree($dir, callable $cb = null)
  {
    $files = scandir($dir);
    $tree = [];
    while($file = array_shift($files)){
      if($file[0]==="."){
        continue;
      }
      $tuple = ['name'=>$file, 'type'=>'file'];
      $tuplePath = $dir.'/'.$file;

      if(is_dir($tuplePath)){
        $tuple['type'] = 'dir';
        $tuple['children'] = $this->buildTree($tuplePath, $cb);
      }

      // exclude autoload file
      if(!in_array($tuplePath, [$this->autoloading_file])) {
        $tree[] = $tuple;
      }

      if(is_callable($cb)){
        call_user_func($cb, $tuple, $tuplePath);
      }

    }
    return $tree;
  }

  private function cleanUpEmptyFolders(array $tree)
  {
    $cleanTree = [];
    while($tuple = array_shift($tree)){
      if($tuple['type'] === 'dir' && empty($tuple['children'])){
        continue;
      }
      $tmpTuple = $tuple;
      if(is_array($tuple['children'])){
        $tmpTuple['children'] = $this->cleanUpEmptyFolders($tuple['children']);
      }
      $cleanTree[] = $tmpTuple;

    }

    return $cleanTree;
  }

  private function sortFoldersFirst(array $tree = [])
  {
      usort($tree, function($current, $next) {

        $makeDirFirst = ($current['type'] === "file") && ($next['type'] === "dir")
        ? true
        : false;
        $makeDirLexicographic = ($current['type'] === "dir") && ($next['type'] === "dir") && ($current['name'] > $next['name'])
        ? true
        : false;
        return $makeDirFirst || $makeDirLexicographic;
      });

      for ($i=0; $i<sizeof($tree); $i++) {
        if(is_array($tree[$i]['children'])){
          $tree[$i]['children'] = $this->sortFoldersFirst($tree[$i]['children']);
        }
      }

      return $tree;
  }


}
