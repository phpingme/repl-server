<?php

namespace Phpingme\ReplServer\Modes;

class Repl extends AbstractModeEnv
{

  public function __invoke(array $data){

    $code = $data['code'];

    $fixture = $data['meta']['fixture'] ?? "";

    $this->env->initExecFile($fixture);

    // filter the optional ";" out
    preg_match('/(.*?);?\s?$/', $code, $match);
    $toEval = $match[1];
    return 'var_dump('.$toEval.');';

  }




}
