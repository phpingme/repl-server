<?php

namespace Phpingme\ReplServer\Modes;

class Terminal extends AbstractModeEnv
{

  public function __invoke(array $data)
  {
    $code = $data['code'];

    return '
    chdir("'.$this->env->home_folder.'");
    $descr = array(
        0 => array(
            \'pipe\',
            \'r\'
        ) ,
        1 => array(
            \'pipe\',
            \'w\'
        ) ,
        2 => array(
            \'pipe\',
            \'w\'
        )
    );
    $pipes = array();
    $process = proc_open("'.$code.'", $descr, $pipes);
    if (is_resource($process)) {
        while ($f = fgets($pipes[1])) {
            echo $f;
        }
        fclose($pipes[1]);
        while ($f = fgets($pipes[2])) {
            echo $f;
        }
        fclose($pipes[2]);
        proc_close($process);
    }

    ';
  }

}
