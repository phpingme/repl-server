<?php

namespace Phpingme\ReplServer\Modes;

class Editor extends AbstractModeEnv
{

  public function __invoke(array $data){

    $code = $data['code'];

    preg_match('/(\<\?php)?(.*)/s', $code, $match);

    $_toEval = $match[2];

    return $_toEval;

  }

}
