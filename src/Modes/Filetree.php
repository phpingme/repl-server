<?php

namespace Phpingme\ReplServer\Modes;

class Filetree extends AbstractModeEnv
{

  public function __invoke(array $data)
  {

    $payload = $data['code'];
    $session = $data['session'];



    switch (strtolower($payload['action'])) {
      case 'create_file':
       return 'echo ((new Phpingme\ReplServer\SandboxEnv("'.$session.'"))->createFile("'.$payload['path'].'"));';
      case 'delete_file':
       return 'echo ((new Phpingme\ReplServer\SandboxEnv("'.$session.'"))->deleteFile("'.$payload['path'].'"));';
      case 'read_file':
       return 'echo json_encode((new Phpingme\ReplServer\SandboxEnv("'.$session.'"))->readFile("'.$payload['path'].'"));';
      case 'save_file':
        return '$c = <<<\'EOT\'
'.$payload['content'].'
EOT;
echo ((new Phpingme\ReplServer\SandboxEnv("'.$session.'"))->saveFileContent("'.$payload['path'].'", $c));';
      break;
      case 'filetree':
       return 'echo json_encode((new Phpingme\ReplServer\SandboxEnv("'.$session.'"))->buildFileTree());';
      case 'export_filetree':
        return 'echo json_encode((new Phpingme\ReplServer\SandboxEnv("'.$session.'"))->serializeFileTree());';
    }
  }

}
