<?php

namespace Phpingme\ReplServer;

use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;


class WebSocket implements MessageComponentInterface {


    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later

        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg) {

        $numRecv = count($this->clients) - 1;
        echo sprintf('Connection %d sending message "%s"' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');

        $result = (new MessageInterpreter)($msg);
        $from->send($result);
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $conn->close();
        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}
