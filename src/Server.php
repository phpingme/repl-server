<?php

namespace Phpingme\ReplServer;

use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use PhpParser\ParserFactory;

class Server {


  public function __invoke($port = "8080"){

    $ws = new WsServer(new WebSocket);
    $ws->disableVersion(0); // old, bad, protocol version


    $server = IoServer::factory(
            new HttpServer($ws),
            $port
    );

    $server->run();

  }

}
