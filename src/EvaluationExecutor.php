<?php

namespace Phpingme\ReplServer;

use PhpParser\ParserFactory;
use Phpingme\ReplServer\Modes\{Terminal, Editor, Repl, Filetree};

class EvaluationExecutor
{

  const REPL = "repl";
  const EDITOR = "editor";
  const TERMINAL = "terminal";
  const FILETREE = "filetree";



  public function __invoke(array $payload)
  {
     if($errors = $this->getPayloadErrors($payload)){
       return $errors;
     }

     $env = $this->createExecutionFile($payload);
     $command = 'php -r "require(\''.$env->autoloading_file.'\');require(\''.$env->execution_file.'\');"';
     echo $command;
     $result = exec($command, $output);

     return $output;
   }

   protected function createExecutionFile(array $payload){

     $commands = $payload['meta']['commands'] ?? [];
     $mode = $payload['mode'] ?? self::REPL;

     $env = new SandboxEnv($payload['session']);
     $env->symlinkCommands($commands);

     switch ($mode) {
       case self::REPL:
         $toEval = (new Repl($env))($payload);
       break;
       case self::EDITOR:
         $toEval = (new Editor($env))($payload);
       break;
       case self::TERMINAL:
         $toEval = (new Terminal($env))($payload);
       break;
       case self::FILETREE:
         $toEval = (new Filetree($env))($payload);
       break;
       default:
         throw new \Error("Unknown code execution mode '".$mode."'");
       break;
      }

      $env->appendToExecutionFile($toEval);

      return $env;
    }

    private function getPayloadErrors($payload) {

      $parseError = $this->lookForParseError($code);
      if(!empty($parseError)) {
        return $parseError;
      }

      return null;
    }

    private function lookForParseError($code)
    {
      $parser = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);
      try {

        $stmts = $parser->parse($code);

        // $stmts is an array of statement nodes
      } catch (\Exception $e) {

        return ["",'Parse Error: '. $e->getMessage()];
      }

      return [];
    }

    public function __destruct(){
      if(!$this->debug){
        #unlink($this->execFile);
      }
    }

}
