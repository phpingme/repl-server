<?php

namespace Phpingme\ReplServer;

class MessageInterpreter
{

  public function __invoke($msg)
  {
    $data = json_decode($msg, true);

    if(isset($data['op']) && $data['op']=='clone') {
      $data['new-session'] = $data['session'] ?? uniqid();
    }

    if(isset($data['op']) && $data['op']=='eval' && isset($data['code'])){

      $data['value'] = (new EvaluationExecutor())($data);
    }
    $result[] = $data;

    return json_encode($result);
 }

}
